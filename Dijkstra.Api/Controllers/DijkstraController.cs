﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Dijkstra.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DijkstraController : ControllerBase
    {
        private readonly ILogger<DijkstraController> logger;

        public DijkstraController(ILogger<DijkstraController> logger)
        {
            this.logger = logger;
        }

        public IActionResult ShortestPath()
        {
            return Ok("Success");
        }
    }
}
