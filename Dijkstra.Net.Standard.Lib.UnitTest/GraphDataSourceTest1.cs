﻿using System;
using Dijkstra.Lib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Dijkstra.UnitTest
{
    [TestClass]
    //[TestCategory("Integration")]
    public class GraphDataSourceTest1
    {
        [TestMethod]
        public void DataSource_InitGraph_NumberOfNodes()
        {
            Graph source = new Graph();
            source.Vertices.Add(new Vertice
            {
                Name = "A",
                Neighbor = {
                    new Vertex
                    {
                        Name="B"
                        ,Edges=6
                    }
                    ,new Vertex
                    {
                        Name="D"
                        ,Edges=1
                    }
                }
            });

            source.Vertices.Add(new Vertice
            {
                Name = "B",
                Neighbor = {
                    new Vertex
                    {
                        Name="A"
                        ,Edges=6
                    }
                    ,new Vertex
                    {
                        Name="C"
                        ,Edges=5
                    }
                    ,new Vertex
                    {
                        Name="D"
                        ,Edges=2
                    },new Vertex
                    {
                        Name="E"
                        ,Edges=2
                    }
                }
            });

            source.Vertices.Add(new Vertice
            {
                Name = "C",
                Neighbor = {
                    new Vertex
                    {
                        Name="B"
                        ,Edges=5
                    }
                    ,new Vertex
                    {
                        Name="E"
                        ,Edges=5
                    }
                }
            });

            source.Vertices.Add(new Vertice { Name = "D", Neighbor = { new Vertex { Name = "A", Edges = 1 }, new Vertex { Name = "B", Edges = 2 }, new Vertex { Name = "E", Edges = 1 } } });

            source.Vertices.Add(new Vertice { Name = "E", Neighbor = { new Vertex { Name = "B", Edges = 2 }, new Vertex { Name = "C", Edges = 5 }, new Vertex { Name = "D", Edges = 1 } } });
        }

        [TestMethod]
        public void DataSource_AddDuplicateVertice__NodeIsExist()
        {
            Graph dijkstra = new Graph();
            dijkstra.AddVertice("A");
            dijkstra.AddVertice("A");
            Assert.AreEqual(1, dijkstra.Vertices.Where(n => n.Name == "A").Count(), "invalid vertice A");
        }

        [TestMethod]
        public void DataSource_AddVerticeNotHaveNeighbor__NodeIsExist()
        {
            Graph dijkstra = new Graph();
            dijkstra.AddVertice("A");
            dijkstra.AddVertice("B");

            Assert.AreEqual(1, dijkstra.Vertices.Where(n => n.Name == "A").Count(), "invalid vertice A");
            Assert.AreEqual(1, dijkstra.Vertices.Where(n => n.Name == "B").Count(), "invalid vertice B");
        }

        [TestMethod]
        public void DataSource_AddVerticeDuplicateNeighbor_ErrorDuplicate()
        {
            string msg = "";
            try
            {
                Graph dijkstra = new Graph();
                dijkstra.AddVertice("A");
                dijkstra.AddVertice("B");
                dijkstra.AddVertice("A", "B", 1);
                dijkstra.AddVertice("B", "A", 2);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            finally
            {
                Assert.AreEqual("duplicate neighbor", msg);
            }
            
        }

        [TestMethod]
        public void DataSource_AddTwoVertice_NodeIsExist()
        {
            Graph dijkstra = new Graph();
            dijkstra.AddVertice("A", "B", 1);
            dijkstra.AddVertice("B");

            Assert.AreEqual(1, dijkstra.Vertices.Where(n => n.Name == "A").Count(), "invalid vertice A");
            Assert.AreEqual(1, dijkstra.Vertices.Where(n => n.Name == "A").First().Neighbor.Where(n => n.Name == "B").Count(), "invalid neighbor B");
            Assert.AreEqual(1, dijkstra.Vertices.Where(n => n.Name == "B").Count(), "invalid vertice B");
            Assert.AreEqual(1, dijkstra.Vertices.Where(n => n.Name == "B").First().Neighbor.Where(n => n.Name == "A").Count(), "invalid neighbor A");
        }

        [TestMethod]
        public void DataSource_AddVerticeInvalidNeighbor_NodeIsExist()
        {
            Graph dijkstra = new Graph();
            dijkstra.AddVertice("A", "A", 1);
            dijkstra.AddVertice("B", "A", 1);

            Assert.AreEqual(1, dijkstra.Vertices.Where(n => n.Name == "A").Count(), "invalid vertice A");
            Assert.AreEqual(1, dijkstra.Vertices.Where(n => n.Name == "A").First().Neighbor.Where(n => n.Name == "B").Count(), "invalid neighbor B");
            Assert.AreEqual(1, dijkstra.Vertices.Where(n => n.Name == "B").Count(), "invalid vertice B");
            Assert.AreEqual(1, dijkstra.Vertices.Where(n => n.Name == "B").First().Neighbor.Where(n => n.Name == "A").Count(), "invalid neighbor A");
        }

        [TestMethod]
        public void SigmaJsExtension_ParseGraph_NodeIsExist()
        {
            Graph graph = new Graph();
            graph.AddVertice("A", "B", 6);
            graph.AddVertice("A", "D", 1);
            graph.AddVertice("B", "C", 5);
            graph.AddVertice("B", "D", 2);
            graph.AddVertice("B", "E", 2);
            graph.AddVertice("C", "E", 5);
            graph.AddVertice("D", "E", 1);

            SigmaJsExtension extension = new SigmaJsExtension();
            var data = extension.ParseGraph(graph);
        }
    }
}
