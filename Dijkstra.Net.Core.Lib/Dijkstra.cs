﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dijkstra.Lib
{
    public class DijkstraLib
    {
        private Graph datasource = null;
        public Graph DataSource
        {
            get
            {
                if (datasource == null)
                {
                    datasource = new Graph();
                }
                return datasource;
            }
            set
            {
                datasource = value;
            }
        }

        public Dictionary<string, Vertex> ShortestPath(string origin, string destination)
        {
            if (string.IsNullOrEmpty(origin))
            {
                throw new Exception("invalid origin");
            }

            if (string.IsNullOrEmpty(destination))
            {
                throw new Exception("invalid destination");
            }

            if (origin == destination)
            {
                throw new Exception("invalid origin and destination");
            }

            if(DataSource?.Vertices.Count == 0)
            {
                throw new Exception("invalid data source");
            }

            if (DataSource.Vertices.Count(n => n.Name == origin) <= 0)
            {
                throw new Exception("origin not exists");
            }

            if (!DataSource.Vertices.Any(n => n.Name == destination))
            {
                throw new Exception("destination not exists");
            }

            var unvisited = new Dictionary<string, Vertice>();  //  List of unvisited nodes
            var visited = new Dictionary<string, Vertice>();    //  List of visited
            var path = new Dictionary<string, Vertex>();         //  List of edge shortest path

            foreach (var vertice in DataSource.Vertices)                    //  Initialization, Loop all nodes
            {
                if (vertice.Name == origin)                                 //  Set origin  
                {
                    vertice.Previous = "";
                    vertice.Distance = 0;                                   //  Distance from origin to origin
                }
                else
                {
                    vertice.Previous = "";                                  //  Previous node in optimal path from origin
                    vertice.Distance = int.MaxValue;                        //  Unknown distance from origin, set infinity                  
                }
                unvisited.Add(vertice.Name, vertice);                       //  All nodes initially in unvisited nodes
            }

            while (unvisited.Count > 0)
            {
                var sortNode = unvisited.OrderBy(x => x.Value.Distance)
                                .ToDictionary(x => x.Key, x => x.Value);    //  Sort node by   distance
                var minDistance = sortNode.First();                         //  Node with the least distance, will be selected first
                unvisited = sortNode;
                unvisited.Remove(minDistance.Key);                          //  Remove  least distance from unvisited

                foreach (var neighbor in minDistance.Value.Neighbor)        //  Find neighbor distance
                {
                    if (!unvisited.ContainsKey(neighbor.Name))              //  Validate exist node unvisited  
                    {
                        continue;
                    }

                    int alt = minDistance.Value.Distance + neighbor.Edges;  //  Calculated tentative distance 
                    if (alt < unvisited[neighbor.Name].Distance)            //  A shorter path to neighbor has been found
                    {
                        unvisited[neighbor.Name].Distance = alt;
                        unvisited[neighbor.Name].Previous = minDistance.Key;
                    }
                }
                visited.Add(minDistance.Key, minDistance.Value);            //  Add visited
            }

            string previous = destination;                                  //  Shortest path result, First node is destination
                                                                            //  Result is edge of origin to destination  
            while (visited.ContainsKey(previous))
            {
                var nodePrevious = visited[previous].Neighbor               //  Get previous node
                                    .FirstOrDefault(n => n.Name == visited[previous].Previous);

                if (nodePrevious != null)
                {
                    path.Add(previous, nodePrevious);                       //  Add previous node
                    previous = nodePrevious.Name;                           //  Continuous, Find next previous node
                }
                else
                {
                    break;
                }
            }
            return path;                                                    //  Return edge shortest path
        }
    }
}
